class ChocolateBars:
    def __init__(self):
        self.cash = 1000
        self.chocolate = 0
        self.wrappers = 0

    def exchangeWrappers(self):
        self.wrappers -= 4
        self.chocolate += 1
        self.wrappers += 1
        print(f"    Exchanged wrappers. chocolate: {self.chocolate}, cash: {self.cash}, wrappers: {self.wrappers}")

    def buyChocolate(self):
        self.chocolate += 1
        self.wrappers += 1
        self.cash -= 1
        print(f"    Bought chocolate: {self.chocolate}, cash: {self.cash}, wrappers: {self.wrappers}")
        if self.wrappers == 4:
            self.exchangeWrappers()

    def startChocolate(self):
        print(f"    Starting chocolate: {self.chocolate}, cash: {self.cash}, wrappers: {self.wrappers}")
        while self.cash > 0:
            self.buyChocolate()

if __name__ == "__main__":
    c = ChocolateBars()
    c.startChocolate()
