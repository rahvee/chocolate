# Chocolate

Charlie has $1,000. He can buy his favorite chocolate bar for $1. There is also a promotion: for every 4 chocolate wrappers he submits, the store will  give him one chocolate bar. What is the maximum number of chocolate bars Charlie can get?

Results:

    Starting chocolate: 0, cash: 1000, wrappers: 0
    Bought chocolate: 1, cash: 999, wrappers: 1
    Bought chocolate: 2, cash: 998, wrappers: 2
    Bought chocolate: 3, cash: 997, wrappers: 3
    Bought chocolate: 4, cash: 996, wrappers: 4
    Exchanged wrappers. chocolate: 5, cash: 996, wrappers: 1
    Bought chocolate: 6, cash: 995, wrappers: 2
    Bought chocolate: 7, cash: 994, wrappers: 3
    Bought chocolate: 8, cash: 993, wrappers: 4
    Exchanged wrappers. chocolate: 9, cash: 993, wrappers: 1
    Bought chocolate: 10, cash: 992, wrappers: 2
    Bought chocolate: 11, cash: 991, wrappers: 3
    Bought chocolate: 12, cash: 990, wrappers: 4
    Exchanged wrappers. chocolate: 13, cash: 990, wrappers: 1
    Bought chocolate: 14, cash: 989, wrappers: 2
    Bought chocolate: 15, cash: 988, wrappers: 3
    Bought chocolate: 16, cash: 987, wrappers: 4
    Exchanged wrappers. chocolate: 17, cash: 987, wrappers: 1
    Bought chocolate: 18, cash: 986, wrappers: 2
    Bought chocolate: 19, cash: 985, wrappers: 3
    Bought chocolate: 20, cash: 984, wrappers: 4
    Exchanged wrappers. chocolate: 21, cash: 984, wrappers: 1
    Bought chocolate: 22, cash: 983, wrappers: 2
    Bought chocolate: 23, cash: 982, wrappers: 3
    Bought chocolate: 24, cash: 981, wrappers: 4
    Exchanged wrappers. chocolate: 25, cash: 981, wrappers: 1
    Bought chocolate: 26, cash: 980, wrappers: 2
    Bought chocolate: 27, cash: 979, wrappers: 3
    Bought chocolate: 28, cash: 978, wrappers: 4
    Exchanged wrappers. chocolate: 29, cash: 978, wrappers: 1
    Bought chocolate: 30, cash: 977, wrappers: 2
    Bought chocolate: 31, cash: 976, wrappers: 3
    Bought chocolate: 32, cash: 975, wrappers: 4
    Exchanged wrappers. chocolate: 33, cash: 975, wrappers: 1
    Bought chocolate: 34, cash: 974, wrappers: 2
    Bought chocolate: 35, cash: 973, wrappers: 3
    Bought chocolate: 36, cash: 972, wrappers: 4
    Exchanged wrappers. chocolate: 37, cash: 972, wrappers: 1
    Bought chocolate: 38, cash: 971, wrappers: 2
    Bought chocolate: 39, cash: 970, wrappers: 3
    Bought chocolate: 40, cash: 969, wrappers: 4
    Exchanged wrappers. chocolate: 41, cash: 969, wrappers: 1
    Bought chocolate: 42, cash: 968, wrappers: 2
    Bought chocolate: 43, cash: 967, wrappers: 3
    Bought chocolate: 44, cash: 966, wrappers: 4
    Exchanged wrappers. chocolate: 45, cash: 966, wrappers: 1
    Bought chocolate: 46, cash: 965, wrappers: 2
    Bought chocolate: 47, cash: 964, wrappers: 3
    Bought chocolate: 48, cash: 963, wrappers: 4
    Exchanged wrappers. chocolate: 49, cash: 963, wrappers: 1
    Bought chocolate: 50, cash: 962, wrappers: 2
    Bought chocolate: 51, cash: 961, wrappers: 3
    Bought chocolate: 52, cash: 960, wrappers: 4
    Exchanged wrappers. chocolate: 53, cash: 960, wrappers: 1
    Bought chocolate: 54, cash: 959, wrappers: 2
    Bought chocolate: 55, cash: 958, wrappers: 3
    Bought chocolate: 56, cash: 957, wrappers: 4
    Exchanged wrappers. chocolate: 57, cash: 957, wrappers: 1
    Bought chocolate: 58, cash: 956, wrappers: 2
    Bought chocolate: 59, cash: 955, wrappers: 3
    Bought chocolate: 60, cash: 954, wrappers: 4
    Exchanged wrappers. chocolate: 61, cash: 954, wrappers: 1
    Bought chocolate: 62, cash: 953, wrappers: 2
    Bought chocolate: 63, cash: 952, wrappers: 3
    Bought chocolate: 64, cash: 951, wrappers: 4
    Exchanged wrappers. chocolate: 65, cash: 951, wrappers: 1
    Bought chocolate: 66, cash: 950, wrappers: 2
    Bought chocolate: 67, cash: 949, wrappers: 3
    Bought chocolate: 68, cash: 948, wrappers: 4
    Exchanged wrappers. chocolate: 69, cash: 948, wrappers: 1
    Bought chocolate: 70, cash: 947, wrappers: 2
    Bought chocolate: 71, cash: 946, wrappers: 3
    Bought chocolate: 72, cash: 945, wrappers: 4
    Exchanged wrappers. chocolate: 73, cash: 945, wrappers: 1
    Bought chocolate: 74, cash: 944, wrappers: 2
    Bought chocolate: 75, cash: 943, wrappers: 3
    Bought chocolate: 76, cash: 942, wrappers: 4
    Exchanged wrappers. chocolate: 77, cash: 942, wrappers: 1
    Bought chocolate: 78, cash: 941, wrappers: 2
    Bought chocolate: 79, cash: 940, wrappers: 3
    Bought chocolate: 80, cash: 939, wrappers: 4
    Exchanged wrappers. chocolate: 81, cash: 939, wrappers: 1
    Bought chocolate: 82, cash: 938, wrappers: 2
    Bought chocolate: 83, cash: 937, wrappers: 3
    Bought chocolate: 84, cash: 936, wrappers: 4
    Exchanged wrappers. chocolate: 85, cash: 936, wrappers: 1
    Bought chocolate: 86, cash: 935, wrappers: 2
    Bought chocolate: 87, cash: 934, wrappers: 3
    Bought chocolate: 88, cash: 933, wrappers: 4
    Exchanged wrappers. chocolate: 89, cash: 933, wrappers: 1
    Bought chocolate: 90, cash: 932, wrappers: 2
    Bought chocolate: 91, cash: 931, wrappers: 3
    Bought chocolate: 92, cash: 930, wrappers: 4
    Exchanged wrappers. chocolate: 93, cash: 930, wrappers: 1
    Bought chocolate: 94, cash: 929, wrappers: 2
    Bought chocolate: 95, cash: 928, wrappers: 3
    Bought chocolate: 96, cash: 927, wrappers: 4
    Exchanged wrappers. chocolate: 97, cash: 927, wrappers: 1
    Bought chocolate: 98, cash: 926, wrappers: 2
    Bought chocolate: 99, cash: 925, wrappers: 3
    Bought chocolate: 100, cash: 924, wrappers: 4
    Exchanged wrappers. chocolate: 101, cash: 924, wrappers: 1
    Bought chocolate: 102, cash: 923, wrappers: 2
    Bought chocolate: 103, cash: 922, wrappers: 3
    Bought chocolate: 104, cash: 921, wrappers: 4
    Exchanged wrappers. chocolate: 105, cash: 921, wrappers: 1
    Bought chocolate: 106, cash: 920, wrappers: 2
    Bought chocolate: 107, cash: 919, wrappers: 3
    Bought chocolate: 108, cash: 918, wrappers: 4
    Exchanged wrappers. chocolate: 109, cash: 918, wrappers: 1
    Bought chocolate: 110, cash: 917, wrappers: 2
    Bought chocolate: 111, cash: 916, wrappers: 3
    Bought chocolate: 112, cash: 915, wrappers: 4
    Exchanged wrappers. chocolate: 113, cash: 915, wrappers: 1
    Bought chocolate: 114, cash: 914, wrappers: 2
    Bought chocolate: 115, cash: 913, wrappers: 3
    Bought chocolate: 116, cash: 912, wrappers: 4
    Exchanged wrappers. chocolate: 117, cash: 912, wrappers: 1
    Bought chocolate: 118, cash: 911, wrappers: 2
    Bought chocolate: 119, cash: 910, wrappers: 3
    Bought chocolate: 120, cash: 909, wrappers: 4
    Exchanged wrappers. chocolate: 121, cash: 909, wrappers: 1
    Bought chocolate: 122, cash: 908, wrappers: 2
    Bought chocolate: 123, cash: 907, wrappers: 3
    Bought chocolate: 124, cash: 906, wrappers: 4
    Exchanged wrappers. chocolate: 125, cash: 906, wrappers: 1
    Bought chocolate: 126, cash: 905, wrappers: 2
    Bought chocolate: 127, cash: 904, wrappers: 3
    Bought chocolate: 128, cash: 903, wrappers: 4
    Exchanged wrappers. chocolate: 129, cash: 903, wrappers: 1
    Bought chocolate: 130, cash: 902, wrappers: 2
    Bought chocolate: 131, cash: 901, wrappers: 3
    Bought chocolate: 132, cash: 900, wrappers: 4
    Exchanged wrappers. chocolate: 133, cash: 900, wrappers: 1
    Bought chocolate: 134, cash: 899, wrappers: 2
    Bought chocolate: 135, cash: 898, wrappers: 3
    Bought chocolate: 136, cash: 897, wrappers: 4
    Exchanged wrappers. chocolate: 137, cash: 897, wrappers: 1
    Bought chocolate: 138, cash: 896, wrappers: 2
    Bought chocolate: 139, cash: 895, wrappers: 3
    Bought chocolate: 140, cash: 894, wrappers: 4
    Exchanged wrappers. chocolate: 141, cash: 894, wrappers: 1
    Bought chocolate: 142, cash: 893, wrappers: 2
    Bought chocolate: 143, cash: 892, wrappers: 3
    Bought chocolate: 144, cash: 891, wrappers: 4
    Exchanged wrappers. chocolate: 145, cash: 891, wrappers: 1
    Bought chocolate: 146, cash: 890, wrappers: 2
    Bought chocolate: 147, cash: 889, wrappers: 3
    Bought chocolate: 148, cash: 888, wrappers: 4
    Exchanged wrappers. chocolate: 149, cash: 888, wrappers: 1
    Bought chocolate: 150, cash: 887, wrappers: 2
    Bought chocolate: 151, cash: 886, wrappers: 3
    Bought chocolate: 152, cash: 885, wrappers: 4
    Exchanged wrappers. chocolate: 153, cash: 885, wrappers: 1
    Bought chocolate: 154, cash: 884, wrappers: 2
    Bought chocolate: 155, cash: 883, wrappers: 3
    Bought chocolate: 156, cash: 882, wrappers: 4
    Exchanged wrappers. chocolate: 157, cash: 882, wrappers: 1
    Bought chocolate: 158, cash: 881, wrappers: 2
    Bought chocolate: 159, cash: 880, wrappers: 3
    Bought chocolate: 160, cash: 879, wrappers: 4
    Exchanged wrappers. chocolate: 161, cash: 879, wrappers: 1
    Bought chocolate: 162, cash: 878, wrappers: 2
    Bought chocolate: 163, cash: 877, wrappers: 3
    Bought chocolate: 164, cash: 876, wrappers: 4
    Exchanged wrappers. chocolate: 165, cash: 876, wrappers: 1
    Bought chocolate: 166, cash: 875, wrappers: 2
    Bought chocolate: 167, cash: 874, wrappers: 3
    Bought chocolate: 168, cash: 873, wrappers: 4
    Exchanged wrappers. chocolate: 169, cash: 873, wrappers: 1
    Bought chocolate: 170, cash: 872, wrappers: 2
    Bought chocolate: 171, cash: 871, wrappers: 3
    Bought chocolate: 172, cash: 870, wrappers: 4
    Exchanged wrappers. chocolate: 173, cash: 870, wrappers: 1
    Bought chocolate: 174, cash: 869, wrappers: 2
    Bought chocolate: 175, cash: 868, wrappers: 3
    Bought chocolate: 176, cash: 867, wrappers: 4
    Exchanged wrappers. chocolate: 177, cash: 867, wrappers: 1
    Bought chocolate: 178, cash: 866, wrappers: 2
    Bought chocolate: 179, cash: 865, wrappers: 3
    Bought chocolate: 180, cash: 864, wrappers: 4
    Exchanged wrappers. chocolate: 181, cash: 864, wrappers: 1
    Bought chocolate: 182, cash: 863, wrappers: 2
    Bought chocolate: 183, cash: 862, wrappers: 3
    Bought chocolate: 184, cash: 861, wrappers: 4
    Exchanged wrappers. chocolate: 185, cash: 861, wrappers: 1
    Bought chocolate: 186, cash: 860, wrappers: 2
    Bought chocolate: 187, cash: 859, wrappers: 3
    Bought chocolate: 188, cash: 858, wrappers: 4
    Exchanged wrappers. chocolate: 189, cash: 858, wrappers: 1
    Bought chocolate: 190, cash: 857, wrappers: 2
    Bought chocolate: 191, cash: 856, wrappers: 3
    Bought chocolate: 192, cash: 855, wrappers: 4
    Exchanged wrappers. chocolate: 193, cash: 855, wrappers: 1
    Bought chocolate: 194, cash: 854, wrappers: 2
    Bought chocolate: 195, cash: 853, wrappers: 3
    Bought chocolate: 196, cash: 852, wrappers: 4
    Exchanged wrappers. chocolate: 197, cash: 852, wrappers: 1
    Bought chocolate: 198, cash: 851, wrappers: 2
    Bought chocolate: 199, cash: 850, wrappers: 3
    Bought chocolate: 200, cash: 849, wrappers: 4
    Exchanged wrappers. chocolate: 201, cash: 849, wrappers: 1
    Bought chocolate: 202, cash: 848, wrappers: 2
    Bought chocolate: 203, cash: 847, wrappers: 3
    Bought chocolate: 204, cash: 846, wrappers: 4
    Exchanged wrappers. chocolate: 205, cash: 846, wrappers: 1
    Bought chocolate: 206, cash: 845, wrappers: 2
    Bought chocolate: 207, cash: 844, wrappers: 3
    Bought chocolate: 208, cash: 843, wrappers: 4
    Exchanged wrappers. chocolate: 209, cash: 843, wrappers: 1
    Bought chocolate: 210, cash: 842, wrappers: 2
    Bought chocolate: 211, cash: 841, wrappers: 3
    Bought chocolate: 212, cash: 840, wrappers: 4
    Exchanged wrappers. chocolate: 213, cash: 840, wrappers: 1
    Bought chocolate: 214, cash: 839, wrappers: 2
    Bought chocolate: 215, cash: 838, wrappers: 3
    Bought chocolate: 216, cash: 837, wrappers: 4
    Exchanged wrappers. chocolate: 217, cash: 837, wrappers: 1
    Bought chocolate: 218, cash: 836, wrappers: 2
    Bought chocolate: 219, cash: 835, wrappers: 3
    Bought chocolate: 220, cash: 834, wrappers: 4
    Exchanged wrappers. chocolate: 221, cash: 834, wrappers: 1
    Bought chocolate: 222, cash: 833, wrappers: 2
    Bought chocolate: 223, cash: 832, wrappers: 3
    Bought chocolate: 224, cash: 831, wrappers: 4
    Exchanged wrappers. chocolate: 225, cash: 831, wrappers: 1
    Bought chocolate: 226, cash: 830, wrappers: 2
    Bought chocolate: 227, cash: 829, wrappers: 3
    Bought chocolate: 228, cash: 828, wrappers: 4
    Exchanged wrappers. chocolate: 229, cash: 828, wrappers: 1
    Bought chocolate: 230, cash: 827, wrappers: 2
    Bought chocolate: 231, cash: 826, wrappers: 3
    Bought chocolate: 232, cash: 825, wrappers: 4
    Exchanged wrappers. chocolate: 233, cash: 825, wrappers: 1
    Bought chocolate: 234, cash: 824, wrappers: 2
    Bought chocolate: 235, cash: 823, wrappers: 3
    Bought chocolate: 236, cash: 822, wrappers: 4
    Exchanged wrappers. chocolate: 237, cash: 822, wrappers: 1
    Bought chocolate: 238, cash: 821, wrappers: 2
    Bought chocolate: 239, cash: 820, wrappers: 3
    Bought chocolate: 240, cash: 819, wrappers: 4
    Exchanged wrappers. chocolate: 241, cash: 819, wrappers: 1
    Bought chocolate: 242, cash: 818, wrappers: 2
    Bought chocolate: 243, cash: 817, wrappers: 3
    Bought chocolate: 244, cash: 816, wrappers: 4
    Exchanged wrappers. chocolate: 245, cash: 816, wrappers: 1
    Bought chocolate: 246, cash: 815, wrappers: 2
    Bought chocolate: 247, cash: 814, wrappers: 3
    Bought chocolate: 248, cash: 813, wrappers: 4
    Exchanged wrappers. chocolate: 249, cash: 813, wrappers: 1
    Bought chocolate: 250, cash: 812, wrappers: 2
    Bought chocolate: 251, cash: 811, wrappers: 3
    Bought chocolate: 252, cash: 810, wrappers: 4
    Exchanged wrappers. chocolate: 253, cash: 810, wrappers: 1
    Bought chocolate: 254, cash: 809, wrappers: 2
    Bought chocolate: 255, cash: 808, wrappers: 3
    Bought chocolate: 256, cash: 807, wrappers: 4
    Exchanged wrappers. chocolate: 257, cash: 807, wrappers: 1
    Bought chocolate: 258, cash: 806, wrappers: 2
    Bought chocolate: 259, cash: 805, wrappers: 3
    Bought chocolate: 260, cash: 804, wrappers: 4
    Exchanged wrappers. chocolate: 261, cash: 804, wrappers: 1
    Bought chocolate: 262, cash: 803, wrappers: 2
    Bought chocolate: 263, cash: 802, wrappers: 3
    Bought chocolate: 264, cash: 801, wrappers: 4
    Exchanged wrappers. chocolate: 265, cash: 801, wrappers: 1
    Bought chocolate: 266, cash: 800, wrappers: 2
    Bought chocolate: 267, cash: 799, wrappers: 3
    Bought chocolate: 268, cash: 798, wrappers: 4
    Exchanged wrappers. chocolate: 269, cash: 798, wrappers: 1
    Bought chocolate: 270, cash: 797, wrappers: 2
    Bought chocolate: 271, cash: 796, wrappers: 3
    Bought chocolate: 272, cash: 795, wrappers: 4
    Exchanged wrappers. chocolate: 273, cash: 795, wrappers: 1
    Bought chocolate: 274, cash: 794, wrappers: 2
    Bought chocolate: 275, cash: 793, wrappers: 3
    Bought chocolate: 276, cash: 792, wrappers: 4
    Exchanged wrappers. chocolate: 277, cash: 792, wrappers: 1
    Bought chocolate: 278, cash: 791, wrappers: 2
    Bought chocolate: 279, cash: 790, wrappers: 3
    Bought chocolate: 280, cash: 789, wrappers: 4
    Exchanged wrappers. chocolate: 281, cash: 789, wrappers: 1
    Bought chocolate: 282, cash: 788, wrappers: 2
    Bought chocolate: 283, cash: 787, wrappers: 3
    Bought chocolate: 284, cash: 786, wrappers: 4
    Exchanged wrappers. chocolate: 285, cash: 786, wrappers: 1
    Bought chocolate: 286, cash: 785, wrappers: 2
    Bought chocolate: 287, cash: 784, wrappers: 3
    Bought chocolate: 288, cash: 783, wrappers: 4
    Exchanged wrappers. chocolate: 289, cash: 783, wrappers: 1
    Bought chocolate: 290, cash: 782, wrappers: 2
    Bought chocolate: 291, cash: 781, wrappers: 3
    Bought chocolate: 292, cash: 780, wrappers: 4
    Exchanged wrappers. chocolate: 293, cash: 780, wrappers: 1
    Bought chocolate: 294, cash: 779, wrappers: 2
    Bought chocolate: 295, cash: 778, wrappers: 3
    Bought chocolate: 296, cash: 777, wrappers: 4
    Exchanged wrappers. chocolate: 297, cash: 777, wrappers: 1
    Bought chocolate: 298, cash: 776, wrappers: 2
    Bought chocolate: 299, cash: 775, wrappers: 3
    Bought chocolate: 300, cash: 774, wrappers: 4
    Exchanged wrappers. chocolate: 301, cash: 774, wrappers: 1
    Bought chocolate: 302, cash: 773, wrappers: 2
    Bought chocolate: 303, cash: 772, wrappers: 3
    Bought chocolate: 304, cash: 771, wrappers: 4
    Exchanged wrappers. chocolate: 305, cash: 771, wrappers: 1
    Bought chocolate: 306, cash: 770, wrappers: 2
    Bought chocolate: 307, cash: 769, wrappers: 3
    Bought chocolate: 308, cash: 768, wrappers: 4
    Exchanged wrappers. chocolate: 309, cash: 768, wrappers: 1
    Bought chocolate: 310, cash: 767, wrappers: 2
    Bought chocolate: 311, cash: 766, wrappers: 3
    Bought chocolate: 312, cash: 765, wrappers: 4
    Exchanged wrappers. chocolate: 313, cash: 765, wrappers: 1
    Bought chocolate: 314, cash: 764, wrappers: 2
    Bought chocolate: 315, cash: 763, wrappers: 3
    Bought chocolate: 316, cash: 762, wrappers: 4
    Exchanged wrappers. chocolate: 317, cash: 762, wrappers: 1
    Bought chocolate: 318, cash: 761, wrappers: 2
    Bought chocolate: 319, cash: 760, wrappers: 3
    Bought chocolate: 320, cash: 759, wrappers: 4
    Exchanged wrappers. chocolate: 321, cash: 759, wrappers: 1
    Bought chocolate: 322, cash: 758, wrappers: 2
    Bought chocolate: 323, cash: 757, wrappers: 3
    Bought chocolate: 324, cash: 756, wrappers: 4
    Exchanged wrappers. chocolate: 325, cash: 756, wrappers: 1
    Bought chocolate: 326, cash: 755, wrappers: 2
    Bought chocolate: 327, cash: 754, wrappers: 3
    Bought chocolate: 328, cash: 753, wrappers: 4
    Exchanged wrappers. chocolate: 329, cash: 753, wrappers: 1
    Bought chocolate: 330, cash: 752, wrappers: 2
    Bought chocolate: 331, cash: 751, wrappers: 3
    Bought chocolate: 332, cash: 750, wrappers: 4
    Exchanged wrappers. chocolate: 333, cash: 750, wrappers: 1
    Bought chocolate: 334, cash: 749, wrappers: 2
    Bought chocolate: 335, cash: 748, wrappers: 3
    Bought chocolate: 336, cash: 747, wrappers: 4
    Exchanged wrappers. chocolate: 337, cash: 747, wrappers: 1
    Bought chocolate: 338, cash: 746, wrappers: 2
    Bought chocolate: 339, cash: 745, wrappers: 3
    Bought chocolate: 340, cash: 744, wrappers: 4
    Exchanged wrappers. chocolate: 341, cash: 744, wrappers: 1
    Bought chocolate: 342, cash: 743, wrappers: 2
    Bought chocolate: 343, cash: 742, wrappers: 3
    Bought chocolate: 344, cash: 741, wrappers: 4
    Exchanged wrappers. chocolate: 345, cash: 741, wrappers: 1
    Bought chocolate: 346, cash: 740, wrappers: 2
    Bought chocolate: 347, cash: 739, wrappers: 3
    Bought chocolate: 348, cash: 738, wrappers: 4
    Exchanged wrappers. chocolate: 349, cash: 738, wrappers: 1
    Bought chocolate: 350, cash: 737, wrappers: 2
    Bought chocolate: 351, cash: 736, wrappers: 3
    Bought chocolate: 352, cash: 735, wrappers: 4
    Exchanged wrappers. chocolate: 353, cash: 735, wrappers: 1
    Bought chocolate: 354, cash: 734, wrappers: 2
    Bought chocolate: 355, cash: 733, wrappers: 3
    Bought chocolate: 356, cash: 732, wrappers: 4
    Exchanged wrappers. chocolate: 357, cash: 732, wrappers: 1
    Bought chocolate: 358, cash: 731, wrappers: 2
    Bought chocolate: 359, cash: 730, wrappers: 3
    Bought chocolate: 360, cash: 729, wrappers: 4
    Exchanged wrappers. chocolate: 361, cash: 729, wrappers: 1
    Bought chocolate: 362, cash: 728, wrappers: 2
    Bought chocolate: 363, cash: 727, wrappers: 3
    Bought chocolate: 364, cash: 726, wrappers: 4
    Exchanged wrappers. chocolate: 365, cash: 726, wrappers: 1
    Bought chocolate: 366, cash: 725, wrappers: 2
    Bought chocolate: 367, cash: 724, wrappers: 3
    Bought chocolate: 368, cash: 723, wrappers: 4
    Exchanged wrappers. chocolate: 369, cash: 723, wrappers: 1
    Bought chocolate: 370, cash: 722, wrappers: 2
    Bought chocolate: 371, cash: 721, wrappers: 3
    Bought chocolate: 372, cash: 720, wrappers: 4
    Exchanged wrappers. chocolate: 373, cash: 720, wrappers: 1
    Bought chocolate: 374, cash: 719, wrappers: 2
    Bought chocolate: 375, cash: 718, wrappers: 3
    Bought chocolate: 376, cash: 717, wrappers: 4
    Exchanged wrappers. chocolate: 377, cash: 717, wrappers: 1
    Bought chocolate: 378, cash: 716, wrappers: 2
    Bought chocolate: 379, cash: 715, wrappers: 3
    Bought chocolate: 380, cash: 714, wrappers: 4
    Exchanged wrappers. chocolate: 381, cash: 714, wrappers: 1
    Bought chocolate: 382, cash: 713, wrappers: 2
    Bought chocolate: 383, cash: 712, wrappers: 3
    Bought chocolate: 384, cash: 711, wrappers: 4
    Exchanged wrappers. chocolate: 385, cash: 711, wrappers: 1
    Bought chocolate: 386, cash: 710, wrappers: 2
    Bought chocolate: 387, cash: 709, wrappers: 3
    Bought chocolate: 388, cash: 708, wrappers: 4
    Exchanged wrappers. chocolate: 389, cash: 708, wrappers: 1
    Bought chocolate: 390, cash: 707, wrappers: 2
    Bought chocolate: 391, cash: 706, wrappers: 3
    Bought chocolate: 392, cash: 705, wrappers: 4
    Exchanged wrappers. chocolate: 393, cash: 705, wrappers: 1
    Bought chocolate: 394, cash: 704, wrappers: 2
    Bought chocolate: 395, cash: 703, wrappers: 3
    Bought chocolate: 396, cash: 702, wrappers: 4
    Exchanged wrappers. chocolate: 397, cash: 702, wrappers: 1
    Bought chocolate: 398, cash: 701, wrappers: 2
    Bought chocolate: 399, cash: 700, wrappers: 3
    Bought chocolate: 400, cash: 699, wrappers: 4
    Exchanged wrappers. chocolate: 401, cash: 699, wrappers: 1
    Bought chocolate: 402, cash: 698, wrappers: 2
    Bought chocolate: 403, cash: 697, wrappers: 3
    Bought chocolate: 404, cash: 696, wrappers: 4
    Exchanged wrappers. chocolate: 405, cash: 696, wrappers: 1
    Bought chocolate: 406, cash: 695, wrappers: 2
    Bought chocolate: 407, cash: 694, wrappers: 3
    Bought chocolate: 408, cash: 693, wrappers: 4
    Exchanged wrappers. chocolate: 409, cash: 693, wrappers: 1
    Bought chocolate: 410, cash: 692, wrappers: 2
    Bought chocolate: 411, cash: 691, wrappers: 3
    Bought chocolate: 412, cash: 690, wrappers: 4
    Exchanged wrappers. chocolate: 413, cash: 690, wrappers: 1
    Bought chocolate: 414, cash: 689, wrappers: 2
    Bought chocolate: 415, cash: 688, wrappers: 3
    Bought chocolate: 416, cash: 687, wrappers: 4
    Exchanged wrappers. chocolate: 417, cash: 687, wrappers: 1
    Bought chocolate: 418, cash: 686, wrappers: 2
    Bought chocolate: 419, cash: 685, wrappers: 3
    Bought chocolate: 420, cash: 684, wrappers: 4
    Exchanged wrappers. chocolate: 421, cash: 684, wrappers: 1
    Bought chocolate: 422, cash: 683, wrappers: 2
    Bought chocolate: 423, cash: 682, wrappers: 3
    Bought chocolate: 424, cash: 681, wrappers: 4
    Exchanged wrappers. chocolate: 425, cash: 681, wrappers: 1
    Bought chocolate: 426, cash: 680, wrappers: 2
    Bought chocolate: 427, cash: 679, wrappers: 3
    Bought chocolate: 428, cash: 678, wrappers: 4
    Exchanged wrappers. chocolate: 429, cash: 678, wrappers: 1
    Bought chocolate: 430, cash: 677, wrappers: 2
    Bought chocolate: 431, cash: 676, wrappers: 3
    Bought chocolate: 432, cash: 675, wrappers: 4
    Exchanged wrappers. chocolate: 433, cash: 675, wrappers: 1
    Bought chocolate: 434, cash: 674, wrappers: 2
    Bought chocolate: 435, cash: 673, wrappers: 3
    Bought chocolate: 436, cash: 672, wrappers: 4
    Exchanged wrappers. chocolate: 437, cash: 672, wrappers: 1
    Bought chocolate: 438, cash: 671, wrappers: 2
    Bought chocolate: 439, cash: 670, wrappers: 3
    Bought chocolate: 440, cash: 669, wrappers: 4
    Exchanged wrappers. chocolate: 441, cash: 669, wrappers: 1
    Bought chocolate: 442, cash: 668, wrappers: 2
    Bought chocolate: 443, cash: 667, wrappers: 3
    Bought chocolate: 444, cash: 666, wrappers: 4
    Exchanged wrappers. chocolate: 445, cash: 666, wrappers: 1
    Bought chocolate: 446, cash: 665, wrappers: 2
    Bought chocolate: 447, cash: 664, wrappers: 3
    Bought chocolate: 448, cash: 663, wrappers: 4
    Exchanged wrappers. chocolate: 449, cash: 663, wrappers: 1
    Bought chocolate: 450, cash: 662, wrappers: 2
    Bought chocolate: 451, cash: 661, wrappers: 3
    Bought chocolate: 452, cash: 660, wrappers: 4
    Exchanged wrappers. chocolate: 453, cash: 660, wrappers: 1
    Bought chocolate: 454, cash: 659, wrappers: 2
    Bought chocolate: 455, cash: 658, wrappers: 3
    Bought chocolate: 456, cash: 657, wrappers: 4
    Exchanged wrappers. chocolate: 457, cash: 657, wrappers: 1
    Bought chocolate: 458, cash: 656, wrappers: 2
    Bought chocolate: 459, cash: 655, wrappers: 3
    Bought chocolate: 460, cash: 654, wrappers: 4
    Exchanged wrappers. chocolate: 461, cash: 654, wrappers: 1
    Bought chocolate: 462, cash: 653, wrappers: 2
    Bought chocolate: 463, cash: 652, wrappers: 3
    Bought chocolate: 464, cash: 651, wrappers: 4
    Exchanged wrappers. chocolate: 465, cash: 651, wrappers: 1
    Bought chocolate: 466, cash: 650, wrappers: 2
    Bought chocolate: 467, cash: 649, wrappers: 3
    Bought chocolate: 468, cash: 648, wrappers: 4
    Exchanged wrappers. chocolate: 469, cash: 648, wrappers: 1
    Bought chocolate: 470, cash: 647, wrappers: 2
    Bought chocolate: 471, cash: 646, wrappers: 3
    Bought chocolate: 472, cash: 645, wrappers: 4
    Exchanged wrappers. chocolate: 473, cash: 645, wrappers: 1
    Bought chocolate: 474, cash: 644, wrappers: 2
    Bought chocolate: 475, cash: 643, wrappers: 3
    Bought chocolate: 476, cash: 642, wrappers: 4
    Exchanged wrappers. chocolate: 477, cash: 642, wrappers: 1
    Bought chocolate: 478, cash: 641, wrappers: 2
    Bought chocolate: 479, cash: 640, wrappers: 3
    Bought chocolate: 480, cash: 639, wrappers: 4
    Exchanged wrappers. chocolate: 481, cash: 639, wrappers: 1
    Bought chocolate: 482, cash: 638, wrappers: 2
    Bought chocolate: 483, cash: 637, wrappers: 3
    Bought chocolate: 484, cash: 636, wrappers: 4
    Exchanged wrappers. chocolate: 485, cash: 636, wrappers: 1
    Bought chocolate: 486, cash: 635, wrappers: 2
    Bought chocolate: 487, cash: 634, wrappers: 3
    Bought chocolate: 488, cash: 633, wrappers: 4
    Exchanged wrappers. chocolate: 489, cash: 633, wrappers: 1
    Bought chocolate: 490, cash: 632, wrappers: 2
    Bought chocolate: 491, cash: 631, wrappers: 3
    Bought chocolate: 492, cash: 630, wrappers: 4
    Exchanged wrappers. chocolate: 493, cash: 630, wrappers: 1
    Bought chocolate: 494, cash: 629, wrappers: 2
    Bought chocolate: 495, cash: 628, wrappers: 3
    Bought chocolate: 496, cash: 627, wrappers: 4
    Exchanged wrappers. chocolate: 497, cash: 627, wrappers: 1
    Bought chocolate: 498, cash: 626, wrappers: 2
    Bought chocolate: 499, cash: 625, wrappers: 3
    Bought chocolate: 500, cash: 624, wrappers: 4
    Exchanged wrappers. chocolate: 501, cash: 624, wrappers: 1
    Bought chocolate: 502, cash: 623, wrappers: 2
    Bought chocolate: 503, cash: 622, wrappers: 3
    Bought chocolate: 504, cash: 621, wrappers: 4
    Exchanged wrappers. chocolate: 505, cash: 621, wrappers: 1
    Bought chocolate: 506, cash: 620, wrappers: 2
    Bought chocolate: 507, cash: 619, wrappers: 3
    Bought chocolate: 508, cash: 618, wrappers: 4
    Exchanged wrappers. chocolate: 509, cash: 618, wrappers: 1
    Bought chocolate: 510, cash: 617, wrappers: 2
    Bought chocolate: 511, cash: 616, wrappers: 3
    Bought chocolate: 512, cash: 615, wrappers: 4
    Exchanged wrappers. chocolate: 513, cash: 615, wrappers: 1
    Bought chocolate: 514, cash: 614, wrappers: 2
    Bought chocolate: 515, cash: 613, wrappers: 3
    Bought chocolate: 516, cash: 612, wrappers: 4
    Exchanged wrappers. chocolate: 517, cash: 612, wrappers: 1
    Bought chocolate: 518, cash: 611, wrappers: 2
    Bought chocolate: 519, cash: 610, wrappers: 3
    Bought chocolate: 520, cash: 609, wrappers: 4
    Exchanged wrappers. chocolate: 521, cash: 609, wrappers: 1
    Bought chocolate: 522, cash: 608, wrappers: 2
    Bought chocolate: 523, cash: 607, wrappers: 3
    Bought chocolate: 524, cash: 606, wrappers: 4
    Exchanged wrappers. chocolate: 525, cash: 606, wrappers: 1
    Bought chocolate: 526, cash: 605, wrappers: 2
    Bought chocolate: 527, cash: 604, wrappers: 3
    Bought chocolate: 528, cash: 603, wrappers: 4
    Exchanged wrappers. chocolate: 529, cash: 603, wrappers: 1
    Bought chocolate: 530, cash: 602, wrappers: 2
    Bought chocolate: 531, cash: 601, wrappers: 3
    Bought chocolate: 532, cash: 600, wrappers: 4
    Exchanged wrappers. chocolate: 533, cash: 600, wrappers: 1
    Bought chocolate: 534, cash: 599, wrappers: 2
    Bought chocolate: 535, cash: 598, wrappers: 3
    Bought chocolate: 536, cash: 597, wrappers: 4
    Exchanged wrappers. chocolate: 537, cash: 597, wrappers: 1
    Bought chocolate: 538, cash: 596, wrappers: 2
    Bought chocolate: 539, cash: 595, wrappers: 3
    Bought chocolate: 540, cash: 594, wrappers: 4
    Exchanged wrappers. chocolate: 541, cash: 594, wrappers: 1
    Bought chocolate: 542, cash: 593, wrappers: 2
    Bought chocolate: 543, cash: 592, wrappers: 3
    Bought chocolate: 544, cash: 591, wrappers: 4
    Exchanged wrappers. chocolate: 545, cash: 591, wrappers: 1
    Bought chocolate: 546, cash: 590, wrappers: 2
    Bought chocolate: 547, cash: 589, wrappers: 3
    Bought chocolate: 548, cash: 588, wrappers: 4
    Exchanged wrappers. chocolate: 549, cash: 588, wrappers: 1
    Bought chocolate: 550, cash: 587, wrappers: 2
    Bought chocolate: 551, cash: 586, wrappers: 3
    Bought chocolate: 552, cash: 585, wrappers: 4
    Exchanged wrappers. chocolate: 553, cash: 585, wrappers: 1
    Bought chocolate: 554, cash: 584, wrappers: 2
    Bought chocolate: 555, cash: 583, wrappers: 3
    Bought chocolate: 556, cash: 582, wrappers: 4
    Exchanged wrappers. chocolate: 557, cash: 582, wrappers: 1
    Bought chocolate: 558, cash: 581, wrappers: 2
    Bought chocolate: 559, cash: 580, wrappers: 3
    Bought chocolate: 560, cash: 579, wrappers: 4
    Exchanged wrappers. chocolate: 561, cash: 579, wrappers: 1
    Bought chocolate: 562, cash: 578, wrappers: 2
    Bought chocolate: 563, cash: 577, wrappers: 3
    Bought chocolate: 564, cash: 576, wrappers: 4
    Exchanged wrappers. chocolate: 565, cash: 576, wrappers: 1
    Bought chocolate: 566, cash: 575, wrappers: 2
    Bought chocolate: 567, cash: 574, wrappers: 3
    Bought chocolate: 568, cash: 573, wrappers: 4
    Exchanged wrappers. chocolate: 569, cash: 573, wrappers: 1
    Bought chocolate: 570, cash: 572, wrappers: 2
    Bought chocolate: 571, cash: 571, wrappers: 3
    Bought chocolate: 572, cash: 570, wrappers: 4
    Exchanged wrappers. chocolate: 573, cash: 570, wrappers: 1
    Bought chocolate: 574, cash: 569, wrappers: 2
    Bought chocolate: 575, cash: 568, wrappers: 3
    Bought chocolate: 576, cash: 567, wrappers: 4
    Exchanged wrappers. chocolate: 577, cash: 567, wrappers: 1
    Bought chocolate: 578, cash: 566, wrappers: 2
    Bought chocolate: 579, cash: 565, wrappers: 3
    Bought chocolate: 580, cash: 564, wrappers: 4
    Exchanged wrappers. chocolate: 581, cash: 564, wrappers: 1
    Bought chocolate: 582, cash: 563, wrappers: 2
    Bought chocolate: 583, cash: 562, wrappers: 3
    Bought chocolate: 584, cash: 561, wrappers: 4
    Exchanged wrappers. chocolate: 585, cash: 561, wrappers: 1
    Bought chocolate: 586, cash: 560, wrappers: 2
    Bought chocolate: 587, cash: 559, wrappers: 3
    Bought chocolate: 588, cash: 558, wrappers: 4
    Exchanged wrappers. chocolate: 589, cash: 558, wrappers: 1
    Bought chocolate: 590, cash: 557, wrappers: 2
    Bought chocolate: 591, cash: 556, wrappers: 3
    Bought chocolate: 592, cash: 555, wrappers: 4
    Exchanged wrappers. chocolate: 593, cash: 555, wrappers: 1
    Bought chocolate: 594, cash: 554, wrappers: 2
    Bought chocolate: 595, cash: 553, wrappers: 3
    Bought chocolate: 596, cash: 552, wrappers: 4
    Exchanged wrappers. chocolate: 597, cash: 552, wrappers: 1
    Bought chocolate: 598, cash: 551, wrappers: 2
    Bought chocolate: 599, cash: 550, wrappers: 3
    Bought chocolate: 600, cash: 549, wrappers: 4
    Exchanged wrappers. chocolate: 601, cash: 549, wrappers: 1
    Bought chocolate: 602, cash: 548, wrappers: 2
    Bought chocolate: 603, cash: 547, wrappers: 3
    Bought chocolate: 604, cash: 546, wrappers: 4
    Exchanged wrappers. chocolate: 605, cash: 546, wrappers: 1
    Bought chocolate: 606, cash: 545, wrappers: 2
    Bought chocolate: 607, cash: 544, wrappers: 3
    Bought chocolate: 608, cash: 543, wrappers: 4
    Exchanged wrappers. chocolate: 609, cash: 543, wrappers: 1
    Bought chocolate: 610, cash: 542, wrappers: 2
    Bought chocolate: 611, cash: 541, wrappers: 3
    Bought chocolate: 612, cash: 540, wrappers: 4
    Exchanged wrappers. chocolate: 613, cash: 540, wrappers: 1
    Bought chocolate: 614, cash: 539, wrappers: 2
    Bought chocolate: 615, cash: 538, wrappers: 3
    Bought chocolate: 616, cash: 537, wrappers: 4
    Exchanged wrappers. chocolate: 617, cash: 537, wrappers: 1
    Bought chocolate: 618, cash: 536, wrappers: 2
    Bought chocolate: 619, cash: 535, wrappers: 3
    Bought chocolate: 620, cash: 534, wrappers: 4
    Exchanged wrappers. chocolate: 621, cash: 534, wrappers: 1
    Bought chocolate: 622, cash: 533, wrappers: 2
    Bought chocolate: 623, cash: 532, wrappers: 3
    Bought chocolate: 624, cash: 531, wrappers: 4
    Exchanged wrappers. chocolate: 625, cash: 531, wrappers: 1
    Bought chocolate: 626, cash: 530, wrappers: 2
    Bought chocolate: 627, cash: 529, wrappers: 3
    Bought chocolate: 628, cash: 528, wrappers: 4
    Exchanged wrappers. chocolate: 629, cash: 528, wrappers: 1
    Bought chocolate: 630, cash: 527, wrappers: 2
    Bought chocolate: 631, cash: 526, wrappers: 3
    Bought chocolate: 632, cash: 525, wrappers: 4
    Exchanged wrappers. chocolate: 633, cash: 525, wrappers: 1
    Bought chocolate: 634, cash: 524, wrappers: 2
    Bought chocolate: 635, cash: 523, wrappers: 3
    Bought chocolate: 636, cash: 522, wrappers: 4
    Exchanged wrappers. chocolate: 637, cash: 522, wrappers: 1
    Bought chocolate: 638, cash: 521, wrappers: 2
    Bought chocolate: 639, cash: 520, wrappers: 3
    Bought chocolate: 640, cash: 519, wrappers: 4
    Exchanged wrappers. chocolate: 641, cash: 519, wrappers: 1
    Bought chocolate: 642, cash: 518, wrappers: 2
    Bought chocolate: 643, cash: 517, wrappers: 3
    Bought chocolate: 644, cash: 516, wrappers: 4
    Exchanged wrappers. chocolate: 645, cash: 516, wrappers: 1
    Bought chocolate: 646, cash: 515, wrappers: 2
    Bought chocolate: 647, cash: 514, wrappers: 3
    Bought chocolate: 648, cash: 513, wrappers: 4
    Exchanged wrappers. chocolate: 649, cash: 513, wrappers: 1
    Bought chocolate: 650, cash: 512, wrappers: 2
    Bought chocolate: 651, cash: 511, wrappers: 3
    Bought chocolate: 652, cash: 510, wrappers: 4
    Exchanged wrappers. chocolate: 653, cash: 510, wrappers: 1
    Bought chocolate: 654, cash: 509, wrappers: 2
    Bought chocolate: 655, cash: 508, wrappers: 3
    Bought chocolate: 656, cash: 507, wrappers: 4
    Exchanged wrappers. chocolate: 657, cash: 507, wrappers: 1
    Bought chocolate: 658, cash: 506, wrappers: 2
    Bought chocolate: 659, cash: 505, wrappers: 3
    Bought chocolate: 660, cash: 504, wrappers: 4
    Exchanged wrappers. chocolate: 661, cash: 504, wrappers: 1
    Bought chocolate: 662, cash: 503, wrappers: 2
    Bought chocolate: 663, cash: 502, wrappers: 3
    Bought chocolate: 664, cash: 501, wrappers: 4
    Exchanged wrappers. chocolate: 665, cash: 501, wrappers: 1
    Bought chocolate: 666, cash: 500, wrappers: 2
    Bought chocolate: 667, cash: 499, wrappers: 3
    Bought chocolate: 668, cash: 498, wrappers: 4
    Exchanged wrappers. chocolate: 669, cash: 498, wrappers: 1
    Bought chocolate: 670, cash: 497, wrappers: 2
    Bought chocolate: 671, cash: 496, wrappers: 3
    Bought chocolate: 672, cash: 495, wrappers: 4
    Exchanged wrappers. chocolate: 673, cash: 495, wrappers: 1
    Bought chocolate: 674, cash: 494, wrappers: 2
    Bought chocolate: 675, cash: 493, wrappers: 3
    Bought chocolate: 676, cash: 492, wrappers: 4
    Exchanged wrappers. chocolate: 677, cash: 492, wrappers: 1
    Bought chocolate: 678, cash: 491, wrappers: 2
    Bought chocolate: 679, cash: 490, wrappers: 3
    Bought chocolate: 680, cash: 489, wrappers: 4
    Exchanged wrappers. chocolate: 681, cash: 489, wrappers: 1
    Bought chocolate: 682, cash: 488, wrappers: 2
    Bought chocolate: 683, cash: 487, wrappers: 3
    Bought chocolate: 684, cash: 486, wrappers: 4
    Exchanged wrappers. chocolate: 685, cash: 486, wrappers: 1
    Bought chocolate: 686, cash: 485, wrappers: 2
    Bought chocolate: 687, cash: 484, wrappers: 3
    Bought chocolate: 688, cash: 483, wrappers: 4
    Exchanged wrappers. chocolate: 689, cash: 483, wrappers: 1
    Bought chocolate: 690, cash: 482, wrappers: 2
    Bought chocolate: 691, cash: 481, wrappers: 3
    Bought chocolate: 692, cash: 480, wrappers: 4
    Exchanged wrappers. chocolate: 693, cash: 480, wrappers: 1
    Bought chocolate: 694, cash: 479, wrappers: 2
    Bought chocolate: 695, cash: 478, wrappers: 3
    Bought chocolate: 696, cash: 477, wrappers: 4
    Exchanged wrappers. chocolate: 697, cash: 477, wrappers: 1
    Bought chocolate: 698, cash: 476, wrappers: 2
    Bought chocolate: 699, cash: 475, wrappers: 3
    Bought chocolate: 700, cash: 474, wrappers: 4
    Exchanged wrappers. chocolate: 701, cash: 474, wrappers: 1
    Bought chocolate: 702, cash: 473, wrappers: 2
    Bought chocolate: 703, cash: 472, wrappers: 3
    Bought chocolate: 704, cash: 471, wrappers: 4
    Exchanged wrappers. chocolate: 705, cash: 471, wrappers: 1
    Bought chocolate: 706, cash: 470, wrappers: 2
    Bought chocolate: 707, cash: 469, wrappers: 3
    Bought chocolate: 708, cash: 468, wrappers: 4
    Exchanged wrappers. chocolate: 709, cash: 468, wrappers: 1
    Bought chocolate: 710, cash: 467, wrappers: 2
    Bought chocolate: 711, cash: 466, wrappers: 3
    Bought chocolate: 712, cash: 465, wrappers: 4
    Exchanged wrappers. chocolate: 713, cash: 465, wrappers: 1
    Bought chocolate: 714, cash: 464, wrappers: 2
    Bought chocolate: 715, cash: 463, wrappers: 3
    Bought chocolate: 716, cash: 462, wrappers: 4
    Exchanged wrappers. chocolate: 717, cash: 462, wrappers: 1
    Bought chocolate: 718, cash: 461, wrappers: 2
    Bought chocolate: 719, cash: 460, wrappers: 3
    Bought chocolate: 720, cash: 459, wrappers: 4
    Exchanged wrappers. chocolate: 721, cash: 459, wrappers: 1
    Bought chocolate: 722, cash: 458, wrappers: 2
    Bought chocolate: 723, cash: 457, wrappers: 3
    Bought chocolate: 724, cash: 456, wrappers: 4
    Exchanged wrappers. chocolate: 725, cash: 456, wrappers: 1
    Bought chocolate: 726, cash: 455, wrappers: 2
    Bought chocolate: 727, cash: 454, wrappers: 3
    Bought chocolate: 728, cash: 453, wrappers: 4
    Exchanged wrappers. chocolate: 729, cash: 453, wrappers: 1
    Bought chocolate: 730, cash: 452, wrappers: 2
    Bought chocolate: 731, cash: 451, wrappers: 3
    Bought chocolate: 732, cash: 450, wrappers: 4
    Exchanged wrappers. chocolate: 733, cash: 450, wrappers: 1
    Bought chocolate: 734, cash: 449, wrappers: 2
    Bought chocolate: 735, cash: 448, wrappers: 3
    Bought chocolate: 736, cash: 447, wrappers: 4
    Exchanged wrappers. chocolate: 737, cash: 447, wrappers: 1
    Bought chocolate: 738, cash: 446, wrappers: 2
    Bought chocolate: 739, cash: 445, wrappers: 3
    Bought chocolate: 740, cash: 444, wrappers: 4
    Exchanged wrappers. chocolate: 741, cash: 444, wrappers: 1
    Bought chocolate: 742, cash: 443, wrappers: 2
    Bought chocolate: 743, cash: 442, wrappers: 3
    Bought chocolate: 744, cash: 441, wrappers: 4
    Exchanged wrappers. chocolate: 745, cash: 441, wrappers: 1
    Bought chocolate: 746, cash: 440, wrappers: 2
    Bought chocolate: 747, cash: 439, wrappers: 3
    Bought chocolate: 748, cash: 438, wrappers: 4
    Exchanged wrappers. chocolate: 749, cash: 438, wrappers: 1
    Bought chocolate: 750, cash: 437, wrappers: 2
    Bought chocolate: 751, cash: 436, wrappers: 3
    Bought chocolate: 752, cash: 435, wrappers: 4
    Exchanged wrappers. chocolate: 753, cash: 435, wrappers: 1
    Bought chocolate: 754, cash: 434, wrappers: 2
    Bought chocolate: 755, cash: 433, wrappers: 3
    Bought chocolate: 756, cash: 432, wrappers: 4
    Exchanged wrappers. chocolate: 757, cash: 432, wrappers: 1
    Bought chocolate: 758, cash: 431, wrappers: 2
    Bought chocolate: 759, cash: 430, wrappers: 3
    Bought chocolate: 760, cash: 429, wrappers: 4
    Exchanged wrappers. chocolate: 761, cash: 429, wrappers: 1
    Bought chocolate: 762, cash: 428, wrappers: 2
    Bought chocolate: 763, cash: 427, wrappers: 3
    Bought chocolate: 764, cash: 426, wrappers: 4
    Exchanged wrappers. chocolate: 765, cash: 426, wrappers: 1
    Bought chocolate: 766, cash: 425, wrappers: 2
    Bought chocolate: 767, cash: 424, wrappers: 3
    Bought chocolate: 768, cash: 423, wrappers: 4
    Exchanged wrappers. chocolate: 769, cash: 423, wrappers: 1
    Bought chocolate: 770, cash: 422, wrappers: 2
    Bought chocolate: 771, cash: 421, wrappers: 3
    Bought chocolate: 772, cash: 420, wrappers: 4
    Exchanged wrappers. chocolate: 773, cash: 420, wrappers: 1
    Bought chocolate: 774, cash: 419, wrappers: 2
    Bought chocolate: 775, cash: 418, wrappers: 3
    Bought chocolate: 776, cash: 417, wrappers: 4
    Exchanged wrappers. chocolate: 777, cash: 417, wrappers: 1
    Bought chocolate: 778, cash: 416, wrappers: 2
    Bought chocolate: 779, cash: 415, wrappers: 3
    Bought chocolate: 780, cash: 414, wrappers: 4
    Exchanged wrappers. chocolate: 781, cash: 414, wrappers: 1
    Bought chocolate: 782, cash: 413, wrappers: 2
    Bought chocolate: 783, cash: 412, wrappers: 3
    Bought chocolate: 784, cash: 411, wrappers: 4
    Exchanged wrappers. chocolate: 785, cash: 411, wrappers: 1
    Bought chocolate: 786, cash: 410, wrappers: 2
    Bought chocolate: 787, cash: 409, wrappers: 3
    Bought chocolate: 788, cash: 408, wrappers: 4
    Exchanged wrappers. chocolate: 789, cash: 408, wrappers: 1
    Bought chocolate: 790, cash: 407, wrappers: 2
    Bought chocolate: 791, cash: 406, wrappers: 3
    Bought chocolate: 792, cash: 405, wrappers: 4
    Exchanged wrappers. chocolate: 793, cash: 405, wrappers: 1
    Bought chocolate: 794, cash: 404, wrappers: 2
    Bought chocolate: 795, cash: 403, wrappers: 3
    Bought chocolate: 796, cash: 402, wrappers: 4
    Exchanged wrappers. chocolate: 797, cash: 402, wrappers: 1
    Bought chocolate: 798, cash: 401, wrappers: 2
    Bought chocolate: 799, cash: 400, wrappers: 3
    Bought chocolate: 800, cash: 399, wrappers: 4
    Exchanged wrappers. chocolate: 801, cash: 399, wrappers: 1
    Bought chocolate: 802, cash: 398, wrappers: 2
    Bought chocolate: 803, cash: 397, wrappers: 3
    Bought chocolate: 804, cash: 396, wrappers: 4
    Exchanged wrappers. chocolate: 805, cash: 396, wrappers: 1
    Bought chocolate: 806, cash: 395, wrappers: 2
    Bought chocolate: 807, cash: 394, wrappers: 3
    Bought chocolate: 808, cash: 393, wrappers: 4
    Exchanged wrappers. chocolate: 809, cash: 393, wrappers: 1
    Bought chocolate: 810, cash: 392, wrappers: 2
    Bought chocolate: 811, cash: 391, wrappers: 3
    Bought chocolate: 812, cash: 390, wrappers: 4
    Exchanged wrappers. chocolate: 813, cash: 390, wrappers: 1
    Bought chocolate: 814, cash: 389, wrappers: 2
    Bought chocolate: 815, cash: 388, wrappers: 3
    Bought chocolate: 816, cash: 387, wrappers: 4
    Exchanged wrappers. chocolate: 817, cash: 387, wrappers: 1
    Bought chocolate: 818, cash: 386, wrappers: 2
    Bought chocolate: 819, cash: 385, wrappers: 3
    Bought chocolate: 820, cash: 384, wrappers: 4
    Exchanged wrappers. chocolate: 821, cash: 384, wrappers: 1
    Bought chocolate: 822, cash: 383, wrappers: 2
    Bought chocolate: 823, cash: 382, wrappers: 3
    Bought chocolate: 824, cash: 381, wrappers: 4
    Exchanged wrappers. chocolate: 825, cash: 381, wrappers: 1
    Bought chocolate: 826, cash: 380, wrappers: 2
    Bought chocolate: 827, cash: 379, wrappers: 3
    Bought chocolate: 828, cash: 378, wrappers: 4
    Exchanged wrappers. chocolate: 829, cash: 378, wrappers: 1
    Bought chocolate: 830, cash: 377, wrappers: 2
    Bought chocolate: 831, cash: 376, wrappers: 3
    Bought chocolate: 832, cash: 375, wrappers: 4
    Exchanged wrappers. chocolate: 833, cash: 375, wrappers: 1
    Bought chocolate: 834, cash: 374, wrappers: 2
    Bought chocolate: 835, cash: 373, wrappers: 3
    Bought chocolate: 836, cash: 372, wrappers: 4
    Exchanged wrappers. chocolate: 837, cash: 372, wrappers: 1
    Bought chocolate: 838, cash: 371, wrappers: 2
    Bought chocolate: 839, cash: 370, wrappers: 3
    Bought chocolate: 840, cash: 369, wrappers: 4
    Exchanged wrappers. chocolate: 841, cash: 369, wrappers: 1
    Bought chocolate: 842, cash: 368, wrappers: 2
    Bought chocolate: 843, cash: 367, wrappers: 3
    Bought chocolate: 844, cash: 366, wrappers: 4
    Exchanged wrappers. chocolate: 845, cash: 366, wrappers: 1
    Bought chocolate: 846, cash: 365, wrappers: 2
    Bought chocolate: 847, cash: 364, wrappers: 3
    Bought chocolate: 848, cash: 363, wrappers: 4
    Exchanged wrappers. chocolate: 849, cash: 363, wrappers: 1
    Bought chocolate: 850, cash: 362, wrappers: 2
    Bought chocolate: 851, cash: 361, wrappers: 3
    Bought chocolate: 852, cash: 360, wrappers: 4
    Exchanged wrappers. chocolate: 853, cash: 360, wrappers: 1
    Bought chocolate: 854, cash: 359, wrappers: 2
    Bought chocolate: 855, cash: 358, wrappers: 3
    Bought chocolate: 856, cash: 357, wrappers: 4
    Exchanged wrappers. chocolate: 857, cash: 357, wrappers: 1
    Bought chocolate: 858, cash: 356, wrappers: 2
    Bought chocolate: 859, cash: 355, wrappers: 3
    Bought chocolate: 860, cash: 354, wrappers: 4
    Exchanged wrappers. chocolate: 861, cash: 354, wrappers: 1
    Bought chocolate: 862, cash: 353, wrappers: 2
    Bought chocolate: 863, cash: 352, wrappers: 3
    Bought chocolate: 864, cash: 351, wrappers: 4
    Exchanged wrappers. chocolate: 865, cash: 351, wrappers: 1
    Bought chocolate: 866, cash: 350, wrappers: 2
    Bought chocolate: 867, cash: 349, wrappers: 3
    Bought chocolate: 868, cash: 348, wrappers: 4
    Exchanged wrappers. chocolate: 869, cash: 348, wrappers: 1
    Bought chocolate: 870, cash: 347, wrappers: 2
    Bought chocolate: 871, cash: 346, wrappers: 3
    Bought chocolate: 872, cash: 345, wrappers: 4
    Exchanged wrappers. chocolate: 873, cash: 345, wrappers: 1
    Bought chocolate: 874, cash: 344, wrappers: 2
    Bought chocolate: 875, cash: 343, wrappers: 3
    Bought chocolate: 876, cash: 342, wrappers: 4
    Exchanged wrappers. chocolate: 877, cash: 342, wrappers: 1
    Bought chocolate: 878, cash: 341, wrappers: 2
    Bought chocolate: 879, cash: 340, wrappers: 3
    Bought chocolate: 880, cash: 339, wrappers: 4
    Exchanged wrappers. chocolate: 881, cash: 339, wrappers: 1
    Bought chocolate: 882, cash: 338, wrappers: 2
    Bought chocolate: 883, cash: 337, wrappers: 3
    Bought chocolate: 884, cash: 336, wrappers: 4
    Exchanged wrappers. chocolate: 885, cash: 336, wrappers: 1
    Bought chocolate: 886, cash: 335, wrappers: 2
    Bought chocolate: 887, cash: 334, wrappers: 3
    Bought chocolate: 888, cash: 333, wrappers: 4
    Exchanged wrappers. chocolate: 889, cash: 333, wrappers: 1
    Bought chocolate: 890, cash: 332, wrappers: 2
    Bought chocolate: 891, cash: 331, wrappers: 3
    Bought chocolate: 892, cash: 330, wrappers: 4
    Exchanged wrappers. chocolate: 893, cash: 330, wrappers: 1
    Bought chocolate: 894, cash: 329, wrappers: 2
    Bought chocolate: 895, cash: 328, wrappers: 3
    Bought chocolate: 896, cash: 327, wrappers: 4
    Exchanged wrappers. chocolate: 897, cash: 327, wrappers: 1
    Bought chocolate: 898, cash: 326, wrappers: 2
    Bought chocolate: 899, cash: 325, wrappers: 3
    Bought chocolate: 900, cash: 324, wrappers: 4
    Exchanged wrappers. chocolate: 901, cash: 324, wrappers: 1
    Bought chocolate: 902, cash: 323, wrappers: 2
    Bought chocolate: 903, cash: 322, wrappers: 3
    Bought chocolate: 904, cash: 321, wrappers: 4
    Exchanged wrappers. chocolate: 905, cash: 321, wrappers: 1
    Bought chocolate: 906, cash: 320, wrappers: 2
    Bought chocolate: 907, cash: 319, wrappers: 3
    Bought chocolate: 908, cash: 318, wrappers: 4
    Exchanged wrappers. chocolate: 909, cash: 318, wrappers: 1
    Bought chocolate: 910, cash: 317, wrappers: 2
    Bought chocolate: 911, cash: 316, wrappers: 3
    Bought chocolate: 912, cash: 315, wrappers: 4
    Exchanged wrappers. chocolate: 913, cash: 315, wrappers: 1
    Bought chocolate: 914, cash: 314, wrappers: 2
    Bought chocolate: 915, cash: 313, wrappers: 3
    Bought chocolate: 916, cash: 312, wrappers: 4
    Exchanged wrappers. chocolate: 917, cash: 312, wrappers: 1
    Bought chocolate: 918, cash: 311, wrappers: 2
    Bought chocolate: 919, cash: 310, wrappers: 3
    Bought chocolate: 920, cash: 309, wrappers: 4
    Exchanged wrappers. chocolate: 921, cash: 309, wrappers: 1
    Bought chocolate: 922, cash: 308, wrappers: 2
    Bought chocolate: 923, cash: 307, wrappers: 3
    Bought chocolate: 924, cash: 306, wrappers: 4
    Exchanged wrappers. chocolate: 925, cash: 306, wrappers: 1
    Bought chocolate: 926, cash: 305, wrappers: 2
    Bought chocolate: 927, cash: 304, wrappers: 3
    Bought chocolate: 928, cash: 303, wrappers: 4
    Exchanged wrappers. chocolate: 929, cash: 303, wrappers: 1
    Bought chocolate: 930, cash: 302, wrappers: 2
    Bought chocolate: 931, cash: 301, wrappers: 3
    Bought chocolate: 932, cash: 300, wrappers: 4
    Exchanged wrappers. chocolate: 933, cash: 300, wrappers: 1
    Bought chocolate: 934, cash: 299, wrappers: 2
    Bought chocolate: 935, cash: 298, wrappers: 3
    Bought chocolate: 936, cash: 297, wrappers: 4
    Exchanged wrappers. chocolate: 937, cash: 297, wrappers: 1
    Bought chocolate: 938, cash: 296, wrappers: 2
    Bought chocolate: 939, cash: 295, wrappers: 3
    Bought chocolate: 940, cash: 294, wrappers: 4
    Exchanged wrappers. chocolate: 941, cash: 294, wrappers: 1
    Bought chocolate: 942, cash: 293, wrappers: 2
    Bought chocolate: 943, cash: 292, wrappers: 3
    Bought chocolate: 944, cash: 291, wrappers: 4
    Exchanged wrappers. chocolate: 945, cash: 291, wrappers: 1
    Bought chocolate: 946, cash: 290, wrappers: 2
    Bought chocolate: 947, cash: 289, wrappers: 3
    Bought chocolate: 948, cash: 288, wrappers: 4
    Exchanged wrappers. chocolate: 949, cash: 288, wrappers: 1
    Bought chocolate: 950, cash: 287, wrappers: 2
    Bought chocolate: 951, cash: 286, wrappers: 3
    Bought chocolate: 952, cash: 285, wrappers: 4
    Exchanged wrappers. chocolate: 953, cash: 285, wrappers: 1
    Bought chocolate: 954, cash: 284, wrappers: 2
    Bought chocolate: 955, cash: 283, wrappers: 3
    Bought chocolate: 956, cash: 282, wrappers: 4
    Exchanged wrappers. chocolate: 957, cash: 282, wrappers: 1
    Bought chocolate: 958, cash: 281, wrappers: 2
    Bought chocolate: 959, cash: 280, wrappers: 3
    Bought chocolate: 960, cash: 279, wrappers: 4
    Exchanged wrappers. chocolate: 961, cash: 279, wrappers: 1
    Bought chocolate: 962, cash: 278, wrappers: 2
    Bought chocolate: 963, cash: 277, wrappers: 3
    Bought chocolate: 964, cash: 276, wrappers: 4
    Exchanged wrappers. chocolate: 965, cash: 276, wrappers: 1
    Bought chocolate: 966, cash: 275, wrappers: 2
    Bought chocolate: 967, cash: 274, wrappers: 3
    Bought chocolate: 968, cash: 273, wrappers: 4
    Exchanged wrappers. chocolate: 969, cash: 273, wrappers: 1
    Bought chocolate: 970, cash: 272, wrappers: 2
    Bought chocolate: 971, cash: 271, wrappers: 3
    Bought chocolate: 972, cash: 270, wrappers: 4
    Exchanged wrappers. chocolate: 973, cash: 270, wrappers: 1
    Bought chocolate: 974, cash: 269, wrappers: 2
    Bought chocolate: 975, cash: 268, wrappers: 3
    Bought chocolate: 976, cash: 267, wrappers: 4
    Exchanged wrappers. chocolate: 977, cash: 267, wrappers: 1
    Bought chocolate: 978, cash: 266, wrappers: 2
    Bought chocolate: 979, cash: 265, wrappers: 3
    Bought chocolate: 980, cash: 264, wrappers: 4
    Exchanged wrappers. chocolate: 981, cash: 264, wrappers: 1
    Bought chocolate: 982, cash: 263, wrappers: 2
    Bought chocolate: 983, cash: 262, wrappers: 3
    Bought chocolate: 984, cash: 261, wrappers: 4
    Exchanged wrappers. chocolate: 985, cash: 261, wrappers: 1
    Bought chocolate: 986, cash: 260, wrappers: 2
    Bought chocolate: 987, cash: 259, wrappers: 3
    Bought chocolate: 988, cash: 258, wrappers: 4
    Exchanged wrappers. chocolate: 989, cash: 258, wrappers: 1
    Bought chocolate: 990, cash: 257, wrappers: 2
    Bought chocolate: 991, cash: 256, wrappers: 3
    Bought chocolate: 992, cash: 255, wrappers: 4
    Exchanged wrappers. chocolate: 993, cash: 255, wrappers: 1
    Bought chocolate: 994, cash: 254, wrappers: 2
    Bought chocolate: 995, cash: 253, wrappers: 3
    Bought chocolate: 996, cash: 252, wrappers: 4
    Exchanged wrappers. chocolate: 997, cash: 252, wrappers: 1
    Bought chocolate: 998, cash: 251, wrappers: 2
    Bought chocolate: 999, cash: 250, wrappers: 3
    Bought chocolate: 1000, cash: 249, wrappers: 4
    Exchanged wrappers. chocolate: 1001, cash: 249, wrappers: 1
    Bought chocolate: 1002, cash: 248, wrappers: 2
    Bought chocolate: 1003, cash: 247, wrappers: 3
    Bought chocolate: 1004, cash: 246, wrappers: 4
    Exchanged wrappers. chocolate: 1005, cash: 246, wrappers: 1
    Bought chocolate: 1006, cash: 245, wrappers: 2
    Bought chocolate: 1007, cash: 244, wrappers: 3
    Bought chocolate: 1008, cash: 243, wrappers: 4
    Exchanged wrappers. chocolate: 1009, cash: 243, wrappers: 1
    Bought chocolate: 1010, cash: 242, wrappers: 2
    Bought chocolate: 1011, cash: 241, wrappers: 3
    Bought chocolate: 1012, cash: 240, wrappers: 4
    Exchanged wrappers. chocolate: 1013, cash: 240, wrappers: 1
    Bought chocolate: 1014, cash: 239, wrappers: 2
    Bought chocolate: 1015, cash: 238, wrappers: 3
    Bought chocolate: 1016, cash: 237, wrappers: 4
    Exchanged wrappers. chocolate: 1017, cash: 237, wrappers: 1
    Bought chocolate: 1018, cash: 236, wrappers: 2
    Bought chocolate: 1019, cash: 235, wrappers: 3
    Bought chocolate: 1020, cash: 234, wrappers: 4
    Exchanged wrappers. chocolate: 1021, cash: 234, wrappers: 1
    Bought chocolate: 1022, cash: 233, wrappers: 2
    Bought chocolate: 1023, cash: 232, wrappers: 3
    Bought chocolate: 1024, cash: 231, wrappers: 4
    Exchanged wrappers. chocolate: 1025, cash: 231, wrappers: 1
    Bought chocolate: 1026, cash: 230, wrappers: 2
    Bought chocolate: 1027, cash: 229, wrappers: 3
    Bought chocolate: 1028, cash: 228, wrappers: 4
    Exchanged wrappers. chocolate: 1029, cash: 228, wrappers: 1
    Bought chocolate: 1030, cash: 227, wrappers: 2
    Bought chocolate: 1031, cash: 226, wrappers: 3
    Bought chocolate: 1032, cash: 225, wrappers: 4
    Exchanged wrappers. chocolate: 1033, cash: 225, wrappers: 1
    Bought chocolate: 1034, cash: 224, wrappers: 2
    Bought chocolate: 1035, cash: 223, wrappers: 3
    Bought chocolate: 1036, cash: 222, wrappers: 4
    Exchanged wrappers. chocolate: 1037, cash: 222, wrappers: 1
    Bought chocolate: 1038, cash: 221, wrappers: 2
    Bought chocolate: 1039, cash: 220, wrappers: 3
    Bought chocolate: 1040, cash: 219, wrappers: 4
    Exchanged wrappers. chocolate: 1041, cash: 219, wrappers: 1
    Bought chocolate: 1042, cash: 218, wrappers: 2
    Bought chocolate: 1043, cash: 217, wrappers: 3
    Bought chocolate: 1044, cash: 216, wrappers: 4
    Exchanged wrappers. chocolate: 1045, cash: 216, wrappers: 1
    Bought chocolate: 1046, cash: 215, wrappers: 2
    Bought chocolate: 1047, cash: 214, wrappers: 3
    Bought chocolate: 1048, cash: 213, wrappers: 4
    Exchanged wrappers. chocolate: 1049, cash: 213, wrappers: 1
    Bought chocolate: 1050, cash: 212, wrappers: 2
    Bought chocolate: 1051, cash: 211, wrappers: 3
    Bought chocolate: 1052, cash: 210, wrappers: 4
    Exchanged wrappers. chocolate: 1053, cash: 210, wrappers: 1
    Bought chocolate: 1054, cash: 209, wrappers: 2
    Bought chocolate: 1055, cash: 208, wrappers: 3
    Bought chocolate: 1056, cash: 207, wrappers: 4
    Exchanged wrappers. chocolate: 1057, cash: 207, wrappers: 1
    Bought chocolate: 1058, cash: 206, wrappers: 2
    Bought chocolate: 1059, cash: 205, wrappers: 3
    Bought chocolate: 1060, cash: 204, wrappers: 4
    Exchanged wrappers. chocolate: 1061, cash: 204, wrappers: 1
    Bought chocolate: 1062, cash: 203, wrappers: 2
    Bought chocolate: 1063, cash: 202, wrappers: 3
    Bought chocolate: 1064, cash: 201, wrappers: 4
    Exchanged wrappers. chocolate: 1065, cash: 201, wrappers: 1
    Bought chocolate: 1066, cash: 200, wrappers: 2
    Bought chocolate: 1067, cash: 199, wrappers: 3
    Bought chocolate: 1068, cash: 198, wrappers: 4
    Exchanged wrappers. chocolate: 1069, cash: 198, wrappers: 1
    Bought chocolate: 1070, cash: 197, wrappers: 2
    Bought chocolate: 1071, cash: 196, wrappers: 3
    Bought chocolate: 1072, cash: 195, wrappers: 4
    Exchanged wrappers. chocolate: 1073, cash: 195, wrappers: 1
    Bought chocolate: 1074, cash: 194, wrappers: 2
    Bought chocolate: 1075, cash: 193, wrappers: 3
    Bought chocolate: 1076, cash: 192, wrappers: 4
    Exchanged wrappers. chocolate: 1077, cash: 192, wrappers: 1
    Bought chocolate: 1078, cash: 191, wrappers: 2
    Bought chocolate: 1079, cash: 190, wrappers: 3
    Bought chocolate: 1080, cash: 189, wrappers: 4
    Exchanged wrappers. chocolate: 1081, cash: 189, wrappers: 1
    Bought chocolate: 1082, cash: 188, wrappers: 2
    Bought chocolate: 1083, cash: 187, wrappers: 3
    Bought chocolate: 1084, cash: 186, wrappers: 4
    Exchanged wrappers. chocolate: 1085, cash: 186, wrappers: 1
    Bought chocolate: 1086, cash: 185, wrappers: 2
    Bought chocolate: 1087, cash: 184, wrappers: 3
    Bought chocolate: 1088, cash: 183, wrappers: 4
    Exchanged wrappers. chocolate: 1089, cash: 183, wrappers: 1
    Bought chocolate: 1090, cash: 182, wrappers: 2
    Bought chocolate: 1091, cash: 181, wrappers: 3
    Bought chocolate: 1092, cash: 180, wrappers: 4
    Exchanged wrappers. chocolate: 1093, cash: 180, wrappers: 1
    Bought chocolate: 1094, cash: 179, wrappers: 2
    Bought chocolate: 1095, cash: 178, wrappers: 3
    Bought chocolate: 1096, cash: 177, wrappers: 4
    Exchanged wrappers. chocolate: 1097, cash: 177, wrappers: 1
    Bought chocolate: 1098, cash: 176, wrappers: 2
    Bought chocolate: 1099, cash: 175, wrappers: 3
    Bought chocolate: 1100, cash: 174, wrappers: 4
    Exchanged wrappers. chocolate: 1101, cash: 174, wrappers: 1
    Bought chocolate: 1102, cash: 173, wrappers: 2
    Bought chocolate: 1103, cash: 172, wrappers: 3
    Bought chocolate: 1104, cash: 171, wrappers: 4
    Exchanged wrappers. chocolate: 1105, cash: 171, wrappers: 1
    Bought chocolate: 1106, cash: 170, wrappers: 2
    Bought chocolate: 1107, cash: 169, wrappers: 3
    Bought chocolate: 1108, cash: 168, wrappers: 4
    Exchanged wrappers. chocolate: 1109, cash: 168, wrappers: 1
    Bought chocolate: 1110, cash: 167, wrappers: 2
    Bought chocolate: 1111, cash: 166, wrappers: 3
    Bought chocolate: 1112, cash: 165, wrappers: 4
    Exchanged wrappers. chocolate: 1113, cash: 165, wrappers: 1
    Bought chocolate: 1114, cash: 164, wrappers: 2
    Bought chocolate: 1115, cash: 163, wrappers: 3
    Bought chocolate: 1116, cash: 162, wrappers: 4
    Exchanged wrappers. chocolate: 1117, cash: 162, wrappers: 1
    Bought chocolate: 1118, cash: 161, wrappers: 2
    Bought chocolate: 1119, cash: 160, wrappers: 3
    Bought chocolate: 1120, cash: 159, wrappers: 4
    Exchanged wrappers. chocolate: 1121, cash: 159, wrappers: 1
    Bought chocolate: 1122, cash: 158, wrappers: 2
    Bought chocolate: 1123, cash: 157, wrappers: 3
    Bought chocolate: 1124, cash: 156, wrappers: 4
    Exchanged wrappers. chocolate: 1125, cash: 156, wrappers: 1
    Bought chocolate: 1126, cash: 155, wrappers: 2
    Bought chocolate: 1127, cash: 154, wrappers: 3
    Bought chocolate: 1128, cash: 153, wrappers: 4
    Exchanged wrappers. chocolate: 1129, cash: 153, wrappers: 1
    Bought chocolate: 1130, cash: 152, wrappers: 2
    Bought chocolate: 1131, cash: 151, wrappers: 3
    Bought chocolate: 1132, cash: 150, wrappers: 4
    Exchanged wrappers. chocolate: 1133, cash: 150, wrappers: 1
    Bought chocolate: 1134, cash: 149, wrappers: 2
    Bought chocolate: 1135, cash: 148, wrappers: 3
    Bought chocolate: 1136, cash: 147, wrappers: 4
    Exchanged wrappers. chocolate: 1137, cash: 147, wrappers: 1
    Bought chocolate: 1138, cash: 146, wrappers: 2
    Bought chocolate: 1139, cash: 145, wrappers: 3
    Bought chocolate: 1140, cash: 144, wrappers: 4
    Exchanged wrappers. chocolate: 1141, cash: 144, wrappers: 1
    Bought chocolate: 1142, cash: 143, wrappers: 2
    Bought chocolate: 1143, cash: 142, wrappers: 3
    Bought chocolate: 1144, cash: 141, wrappers: 4
    Exchanged wrappers. chocolate: 1145, cash: 141, wrappers: 1
    Bought chocolate: 1146, cash: 140, wrappers: 2
    Bought chocolate: 1147, cash: 139, wrappers: 3
    Bought chocolate: 1148, cash: 138, wrappers: 4
    Exchanged wrappers. chocolate: 1149, cash: 138, wrappers: 1
    Bought chocolate: 1150, cash: 137, wrappers: 2
    Bought chocolate: 1151, cash: 136, wrappers: 3
    Bought chocolate: 1152, cash: 135, wrappers: 4
    Exchanged wrappers. chocolate: 1153, cash: 135, wrappers: 1
    Bought chocolate: 1154, cash: 134, wrappers: 2
    Bought chocolate: 1155, cash: 133, wrappers: 3
    Bought chocolate: 1156, cash: 132, wrappers: 4
    Exchanged wrappers. chocolate: 1157, cash: 132, wrappers: 1
    Bought chocolate: 1158, cash: 131, wrappers: 2
    Bought chocolate: 1159, cash: 130, wrappers: 3
    Bought chocolate: 1160, cash: 129, wrappers: 4
    Exchanged wrappers. chocolate: 1161, cash: 129, wrappers: 1
    Bought chocolate: 1162, cash: 128, wrappers: 2
    Bought chocolate: 1163, cash: 127, wrappers: 3
    Bought chocolate: 1164, cash: 126, wrappers: 4
    Exchanged wrappers. chocolate: 1165, cash: 126, wrappers: 1
    Bought chocolate: 1166, cash: 125, wrappers: 2
    Bought chocolate: 1167, cash: 124, wrappers: 3
    Bought chocolate: 1168, cash: 123, wrappers: 4
    Exchanged wrappers. chocolate: 1169, cash: 123, wrappers: 1
    Bought chocolate: 1170, cash: 122, wrappers: 2
    Bought chocolate: 1171, cash: 121, wrappers: 3
    Bought chocolate: 1172, cash: 120, wrappers: 4
    Exchanged wrappers. chocolate: 1173, cash: 120, wrappers: 1
    Bought chocolate: 1174, cash: 119, wrappers: 2
    Bought chocolate: 1175, cash: 118, wrappers: 3
    Bought chocolate: 1176, cash: 117, wrappers: 4
    Exchanged wrappers. chocolate: 1177, cash: 117, wrappers: 1
    Bought chocolate: 1178, cash: 116, wrappers: 2
    Bought chocolate: 1179, cash: 115, wrappers: 3
    Bought chocolate: 1180, cash: 114, wrappers: 4
    Exchanged wrappers. chocolate: 1181, cash: 114, wrappers: 1
    Bought chocolate: 1182, cash: 113, wrappers: 2
    Bought chocolate: 1183, cash: 112, wrappers: 3
    Bought chocolate: 1184, cash: 111, wrappers: 4
    Exchanged wrappers. chocolate: 1185, cash: 111, wrappers: 1
    Bought chocolate: 1186, cash: 110, wrappers: 2
    Bought chocolate: 1187, cash: 109, wrappers: 3
    Bought chocolate: 1188, cash: 108, wrappers: 4
    Exchanged wrappers. chocolate: 1189, cash: 108, wrappers: 1
    Bought chocolate: 1190, cash: 107, wrappers: 2
    Bought chocolate: 1191, cash: 106, wrappers: 3
    Bought chocolate: 1192, cash: 105, wrappers: 4
    Exchanged wrappers. chocolate: 1193, cash: 105, wrappers: 1
    Bought chocolate: 1194, cash: 104, wrappers: 2
    Bought chocolate: 1195, cash: 103, wrappers: 3
    Bought chocolate: 1196, cash: 102, wrappers: 4
    Exchanged wrappers. chocolate: 1197, cash: 102, wrappers: 1
    Bought chocolate: 1198, cash: 101, wrappers: 2
    Bought chocolate: 1199, cash: 100, wrappers: 3
    Bought chocolate: 1200, cash: 99, wrappers: 4
    Exchanged wrappers. chocolate: 1201, cash: 99, wrappers: 1
    Bought chocolate: 1202, cash: 98, wrappers: 2
    Bought chocolate: 1203, cash: 97, wrappers: 3
    Bought chocolate: 1204, cash: 96, wrappers: 4
    Exchanged wrappers. chocolate: 1205, cash: 96, wrappers: 1
    Bought chocolate: 1206, cash: 95, wrappers: 2
    Bought chocolate: 1207, cash: 94, wrappers: 3
    Bought chocolate: 1208, cash: 93, wrappers: 4
    Exchanged wrappers. chocolate: 1209, cash: 93, wrappers: 1
    Bought chocolate: 1210, cash: 92, wrappers: 2
    Bought chocolate: 1211, cash: 91, wrappers: 3
    Bought chocolate: 1212, cash: 90, wrappers: 4
    Exchanged wrappers. chocolate: 1213, cash: 90, wrappers: 1
    Bought chocolate: 1214, cash: 89, wrappers: 2
    Bought chocolate: 1215, cash: 88, wrappers: 3
    Bought chocolate: 1216, cash: 87, wrappers: 4
    Exchanged wrappers. chocolate: 1217, cash: 87, wrappers: 1
    Bought chocolate: 1218, cash: 86, wrappers: 2
    Bought chocolate: 1219, cash: 85, wrappers: 3
    Bought chocolate: 1220, cash: 84, wrappers: 4
    Exchanged wrappers. chocolate: 1221, cash: 84, wrappers: 1
    Bought chocolate: 1222, cash: 83, wrappers: 2
    Bought chocolate: 1223, cash: 82, wrappers: 3
    Bought chocolate: 1224, cash: 81, wrappers: 4
    Exchanged wrappers. chocolate: 1225, cash: 81, wrappers: 1
    Bought chocolate: 1226, cash: 80, wrappers: 2
    Bought chocolate: 1227, cash: 79, wrappers: 3
    Bought chocolate: 1228, cash: 78, wrappers: 4
    Exchanged wrappers. chocolate: 1229, cash: 78, wrappers: 1
    Bought chocolate: 1230, cash: 77, wrappers: 2
    Bought chocolate: 1231, cash: 76, wrappers: 3
    Bought chocolate: 1232, cash: 75, wrappers: 4
    Exchanged wrappers. chocolate: 1233, cash: 75, wrappers: 1
    Bought chocolate: 1234, cash: 74, wrappers: 2
    Bought chocolate: 1235, cash: 73, wrappers: 3
    Bought chocolate: 1236, cash: 72, wrappers: 4
    Exchanged wrappers. chocolate: 1237, cash: 72, wrappers: 1
    Bought chocolate: 1238, cash: 71, wrappers: 2
    Bought chocolate: 1239, cash: 70, wrappers: 3
    Bought chocolate: 1240, cash: 69, wrappers: 4
    Exchanged wrappers. chocolate: 1241, cash: 69, wrappers: 1
    Bought chocolate: 1242, cash: 68, wrappers: 2
    Bought chocolate: 1243, cash: 67, wrappers: 3
    Bought chocolate: 1244, cash: 66, wrappers: 4
    Exchanged wrappers. chocolate: 1245, cash: 66, wrappers: 1
    Bought chocolate: 1246, cash: 65, wrappers: 2
    Bought chocolate: 1247, cash: 64, wrappers: 3
    Bought chocolate: 1248, cash: 63, wrappers: 4
    Exchanged wrappers. chocolate: 1249, cash: 63, wrappers: 1
    Bought chocolate: 1250, cash: 62, wrappers: 2
    Bought chocolate: 1251, cash: 61, wrappers: 3
    Bought chocolate: 1252, cash: 60, wrappers: 4
    Exchanged wrappers. chocolate: 1253, cash: 60, wrappers: 1
    Bought chocolate: 1254, cash: 59, wrappers: 2
    Bought chocolate: 1255, cash: 58, wrappers: 3
    Bought chocolate: 1256, cash: 57, wrappers: 4
    Exchanged wrappers. chocolate: 1257, cash: 57, wrappers: 1
    Bought chocolate: 1258, cash: 56, wrappers: 2
    Bought chocolate: 1259, cash: 55, wrappers: 3
    Bought chocolate: 1260, cash: 54, wrappers: 4
    Exchanged wrappers. chocolate: 1261, cash: 54, wrappers: 1
    Bought chocolate: 1262, cash: 53, wrappers: 2
    Bought chocolate: 1263, cash: 52, wrappers: 3
    Bought chocolate: 1264, cash: 51, wrappers: 4
    Exchanged wrappers. chocolate: 1265, cash: 51, wrappers: 1
    Bought chocolate: 1266, cash: 50, wrappers: 2
    Bought chocolate: 1267, cash: 49, wrappers: 3
    Bought chocolate: 1268, cash: 48, wrappers: 4
    Exchanged wrappers. chocolate: 1269, cash: 48, wrappers: 1
    Bought chocolate: 1270, cash: 47, wrappers: 2
    Bought chocolate: 1271, cash: 46, wrappers: 3
    Bought chocolate: 1272, cash: 45, wrappers: 4
    Exchanged wrappers. chocolate: 1273, cash: 45, wrappers: 1
    Bought chocolate: 1274, cash: 44, wrappers: 2
    Bought chocolate: 1275, cash: 43, wrappers: 3
    Bought chocolate: 1276, cash: 42, wrappers: 4
    Exchanged wrappers. chocolate: 1277, cash: 42, wrappers: 1
    Bought chocolate: 1278, cash: 41, wrappers: 2
    Bought chocolate: 1279, cash: 40, wrappers: 3
    Bought chocolate: 1280, cash: 39, wrappers: 4
    Exchanged wrappers. chocolate: 1281, cash: 39, wrappers: 1
    Bought chocolate: 1282, cash: 38, wrappers: 2
    Bought chocolate: 1283, cash: 37, wrappers: 3
    Bought chocolate: 1284, cash: 36, wrappers: 4
    Exchanged wrappers. chocolate: 1285, cash: 36, wrappers: 1
    Bought chocolate: 1286, cash: 35, wrappers: 2
    Bought chocolate: 1287, cash: 34, wrappers: 3
    Bought chocolate: 1288, cash: 33, wrappers: 4
    Exchanged wrappers. chocolate: 1289, cash: 33, wrappers: 1
    Bought chocolate: 1290, cash: 32, wrappers: 2
    Bought chocolate: 1291, cash: 31, wrappers: 3
    Bought chocolate: 1292, cash: 30, wrappers: 4
    Exchanged wrappers. chocolate: 1293, cash: 30, wrappers: 1
    Bought chocolate: 1294, cash: 29, wrappers: 2
    Bought chocolate: 1295, cash: 28, wrappers: 3
    Bought chocolate: 1296, cash: 27, wrappers: 4
    Exchanged wrappers. chocolate: 1297, cash: 27, wrappers: 1
    Bought chocolate: 1298, cash: 26, wrappers: 2
    Bought chocolate: 1299, cash: 25, wrappers: 3
    Bought chocolate: 1300, cash: 24, wrappers: 4
    Exchanged wrappers. chocolate: 1301, cash: 24, wrappers: 1
    Bought chocolate: 1302, cash: 23, wrappers: 2
    Bought chocolate: 1303, cash: 22, wrappers: 3
    Bought chocolate: 1304, cash: 21, wrappers: 4
    Exchanged wrappers. chocolate: 1305, cash: 21, wrappers: 1
    Bought chocolate: 1306, cash: 20, wrappers: 2
    Bought chocolate: 1307, cash: 19, wrappers: 3
    Bought chocolate: 1308, cash: 18, wrappers: 4
    Exchanged wrappers. chocolate: 1309, cash: 18, wrappers: 1
    Bought chocolate: 1310, cash: 17, wrappers: 2
    Bought chocolate: 1311, cash: 16, wrappers: 3
    Bought chocolate: 1312, cash: 15, wrappers: 4
    Exchanged wrappers. chocolate: 1313, cash: 15, wrappers: 1
    Bought chocolate: 1314, cash: 14, wrappers: 2
    Bought chocolate: 1315, cash: 13, wrappers: 3
    Bought chocolate: 1316, cash: 12, wrappers: 4
    Exchanged wrappers. chocolate: 1317, cash: 12, wrappers: 1
    Bought chocolate: 1318, cash: 11, wrappers: 2
    Bought chocolate: 1319, cash: 10, wrappers: 3
    Bought chocolate: 1320, cash: 9, wrappers: 4
    Exchanged wrappers. chocolate: 1321, cash: 9, wrappers: 1
    Bought chocolate: 1322, cash: 8, wrappers: 2
    Bought chocolate: 1323, cash: 7, wrappers: 3
    Bought chocolate: 1324, cash: 6, wrappers: 4
    Exchanged wrappers. chocolate: 1325, cash: 6, wrappers: 1
    Bought chocolate: 1326, cash: 5, wrappers: 2
    Bought chocolate: 1327, cash: 4, wrappers: 3
    Bought chocolate: 1328, cash: 3, wrappers: 4
    Exchanged wrappers. chocolate: 1329, cash: 3, wrappers: 1
    Bought chocolate: 1330, cash: 2, wrappers: 2
    Bought chocolate: 1331, cash: 1, wrappers: 3
    Bought chocolate: 1332, cash: 0, wrappers: 4
    Exchanged wrappers. chocolate: 1333, cash: 0, wrappers: 1
